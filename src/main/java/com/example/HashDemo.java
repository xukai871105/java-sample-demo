package com.example;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by XUKAI6 on 2017-7-23.
 */
public class HashDemo {
    public void runOnce() {
        Map<String, Integer> employees = new HashMap<>();

        employees.put("lilei", 32);
        employees.put("hanmeimei", 29);
        employees.put("green", 35);

        System.out.println(employees);
        System.out.println("lilei:" + employees.get("lilei"));
        System.out.println(employees.containsKey("fox"));
        System.out.println(employees.keySet());
        System.out.println(employees.isEmpty());

        // 采用Iterator遍历HashMap
        Iterator item = employees.keySet().iterator();
        while(item.hasNext()) {
            String key = (String)item.next();
            System.out.println("key:" + key + " value:" + employees.get(key));
            // System.out.println("value:" + employees.get(key));
        }
    }
}

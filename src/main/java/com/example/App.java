package com.example;

// import com.example.CoAPClientApp;

public class App {
    public static void main( String[] args )
    {
        CoAPClientApp app = new CoAPClientApp();
        app.runOnce("coap://wsncoap.org/test");

        HashDemo demo = new HashDemo();
        demo.runOnce();

        ListDemo listDemo = new ListDemo();
        listDemo.runOnce();
    }
}

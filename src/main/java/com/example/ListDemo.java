package com.example;

import java.util.*;

/**
 * Created by XUKAI6 on 2017-7-23.
 * 同时使用List和Map
 */
public class ListDemo {
    public ListDemo(){
        System.out.println("-----List Demo-----");
    }
    public void runOnce(){
        List<Map<String, Double>> scatter = new ArrayList<>();

        Map<String, Double> pointA = new HashMap<>();
        pointA.put("x", 2.0);
        pointA.put("y", 3.0);
        scatter.add(pointA);

        Map<String, Double> pointB = new HashMap<>();
        pointB.put("x", 4.0);
        pointB.put("y", 5.0);
        scatter.add(pointB);

//        Iterator<Map<String, Double>> it = scatter.iterator();
//        while(it.hasNext()) {
//            Map<String, Double> point = it.next();
//            System.out.print("x: " + point.get("x") + " y:" + point.get("y") + "\n");
//        }

        for (int i = 0; i < scatter.size(); i++) {
            // System.out.print("-----%d-----\n", i);
            System.out.print(String.format("Index: %d\n", i));
            Map<String, Double> point = scatter.get(i);
            System.out.println("x: " + point.get("x"));
            System.out.println("y: " + point.get("y"));
        }

    }
}
